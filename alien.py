import pygame
from pygame.sprite import Sprite


class Alien(Sprite):

    def __init__(self, ai_settings, screen):
        # 初始化一个Bullet对象
        super(Alien, self).__init__()
        self.screen = screen

        # 加载飞船图片
        self.image = pygame.image.load('images/alien.bmp')
        # 获取图片矩阵
        self.rect = self.image.get_rect()
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

    def blitme(self):
        self.screen.blit(self.image, self.rect)
