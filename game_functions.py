import sys
import pygame
from bullet import Bullet


def check_keydown_events(event, ship, ai_settings, screen, bullets):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    elif event.key == pygame.K_LEFT:
        ship.moving_left -= True
    elif event.key == pygame.K_SPACE:
        fire_bullet(ship, ai_settings, screen, bullets)
    elif event.key == pygame.K_q:
        sys.exit()


def check_keyup_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    elif event.key == pygame.K_LEFT:
        ship.moving_left = False


def check_events(ship, ai_settings, screen, bullets):
    # 循环刷新屏幕，如果屏幕刷新展示刷新后的，否则屏幕显示不变
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, ship, ai_settings, screen, bullets)
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, ship)


def update_screen(ai_settings, screen, ship, bullets, alien):
    # 设置背景颜色
    screen.fill(ai_settings.bg_color)
    # 显示子弹
    for bullet in bullets:
        bullet.draw_bullet()
    # 显示外星人
    alien.blitme()
    # 显示飞船
    ship.blitme()
    # 显示屏幕
    pygame.display.flip()


def fire_bullet(ship, ai_settings, screen, bullets):
    if len(bullets) < ai_settings.bullet_limit:
        new_bullet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bullet)


def update_bullets(bullets):
    bullets.update()
    for bullet in bullets.copy():
        if bullet.rect.top < 0:
            bullets.remove(bullet)
