import pygame
from game_functions import check_events, update_screen, update_bullets
from settings import Settings
from ship import Ship
from alien import Alien
from pygame.sprite import Group


def run_game():
    # 初始化背景设置
    pygame.init()

    # 加入设置
    ai_settings = Settings()

    # 创建一个1200像素宽，800像素长的屏幕
    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))
    # 添加飞船
    ship = Ship(screen)

    # 设置显示的窗口名
    pygame.display.set_caption(ai_settings.screen_caption)

    # 添加子弹容器
    bullets = Group()

    # 添加外星人
    alien = Alien(ai_settings, screen)

    # 循环刷新屏幕，如果屏幕刷新展示刷新后的，否则屏幕显示不变
    while True:
        check_events(ship, ai_settings, screen, bullets)
        # 删除已经消失的子弹
        update_bullets(bullets)
        # print(len(bullets))
        ship.update()
        update_screen(ai_settings, screen, ship, bullets, alien)


if __name__ == "__main__":
    run_game()
