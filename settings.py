class Settings():
    def __init__(self):
        # 设置屏幕宽度、高度、标题
        self.screen_width = 1200
        self.screen_height = 800
        self.screen_caption = "Alien Invasion"

        # 设置背景颜色为灰色
        self.bg_color = (230, 230, 230)
        # self.bg_color = (0, 0 ,230)

        # 设置子弹大小、速度、颜色
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_speed_factor = 1.5
        self.bullet_color = 60, 60, 60
        self.bullet_limit = 3
