import pygame
from pygame.sprite import Sprite


class Bullet(Sprite):

    def __init__(self, ai_settings, screen, ship):
        # 初始化一个Bullet对象
        super(Bullet, self).__init__()
        self.screen = screen

        # 设置子弹属性
        self.rect = pygame.Rect(
            0, 0, ai_settings.bullet_width, ai_settings.bullet_height)
        self.rect.centerx = ship.rect.centerx
        self.rect.bottom = ship.rect.bottom
        self.y = float(self.rect.y)

        # 设置子弹速度、颜色
        self.color = ai_settings.bullet_color
        self.speed_factor = ai_settings.bullet_speed_factor

    def update(self):
        # 移动子弹
        self.y -= self.speed_factor
        self.rect.y = self.y

    def draw_bullet(self):
        # 画子弹
        pygame.draw.rect(self.screen, self.color, self.rect)
