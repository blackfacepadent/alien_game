import pygame


class Ship():
    def __init__(self, screen):
        self.screen = screen
        # 加载飞船图片
        self.image = pygame.image.load('images/ship.bmp')
        # 获取图片矩阵
        self.rect = self.image.get_rect()
        # 获取屏幕矩阵
        self.screen_rect = screen.get_rect()
        # 设置飞船移动速度
        self.ship_speed_factor = 1.5

        # 使飞船显示在屏幕底边
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom

        self.moving_right = False
        self.moving_left = False

    # 显示飞船
    def blitme(self):
        self.screen.blit(self.image, self.rect)

    # 更新飞船的位置
    def update(self):
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.rect.centerx += self.ship_speed_factor

        if self.moving_left and self.rect.left > 0:
            self.rect.centerx -= self.ship_speed_factor
